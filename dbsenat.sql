-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2018 at 07:53 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsenat`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_description`) VALUES
(1, 'Smartphone', 'A celular phone for your work.'),
(2, 'Laptop', 'A portable computer for daily uses.'),
(3, 'PC', 'Personal computer for office or server.');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `event_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_speaker` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_fee` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_category` enum('Chapel','Seminar','Workshop','Study Tour','Other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `event_title`, `event_speaker`, `event_description`, `event_fee`, `event_category`, `event_date`) VALUES
(1, 'Ducimus illum voluptatem ullam autem.', 'Ms. Sunny Leffler I', 'For anything tougher than suet; Yet you turned a back-somersault in at the beginning,\' the King replied. Here the Queen shrieked out. \'Behead that Dormouse! Turn that Dormouse out of the hall; but.', '170150', 'Study Tour', '2008-10-30'),
(2, 'Eum reprehenderit ratione incidunt neque aperiam magnam eum.', 'Drake Legros', 'I\'ve finished.\' So they began moving about again, and Alice joined the procession, wondering very much pleased at having found out a race-course, in a shrill, passionate voice. \'Would YOU like cats.', '191754', 'Other', '1998-05-27'),
(3, 'Nobis sunt qui eum minima aut nihil.', 'Miss Chelsea Lemke IV', 'Alice could see, as she could not tell whether they were nice grand words to say.) Presently she began thinking over other children she knew that it was quite impossible to say when I get.', '194766', 'Seminar', '1992-04-21'),
(4, 'Sed nobis similique ullam est aut laudantium.', 'Raphaelle Mills', 'BEST butter, you know.\' It was, no doubt: only Alice did not appear, and after a few minutes, and she tried the little creature down, and nobody spoke for some time without hearing anything more: at.', '172502', 'Chapel', '1975-06-04'),
(5, 'Doloribus est eos sed veniam iure alias.', 'Mylene Cronin', 'Hatter. \'Does YOUR watch tell you what year it is?\' \'Of course not,\' Alice replied very gravely. \'What else have you executed on the top of her ever getting out of sight. Alice remained looking.', '194834', 'Other', '1979-12-03');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(36, '2014_10_12_000000_create_users_table', 1),
(37, '2014_10_12_100000_create_password_resets_table', 1),
(38, '2018_02_07_085812_create_events_table', 1),
(39, '2018_03_09_112032_create_products_table', 1),
(40, '2018_03_10_111012_create_categories_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `prod_id` int(10) UNSIGNED NOT NULL,
  `prod_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prod_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `prod_price` int(11) NOT NULL,
  `prod_qty` int(11) NOT NULL,
  `prod_cat_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`prod_id`, `prod_name`, `prod_description`, `prod_price`, `prod_qty`, `prod_cat_id`) VALUES
(1, 'Samsung Galaxy S9', 'New and modern smartphone.', 11950000, 64, 1),
(2, 'Apple MacBook Air', 'Your best friend for work.', 14500000, 24, 2),
(3, 'Lenovo ThinkCenter Px1250', 'Personal computer for office or server.', 10500000, 128, 3),
(4, 'DELL Latitude ESX 420', 'Personal computer for multimedia.', 17500000, 12, 3),
(5, 'Very New Product', 'Our brand new high quality product.', 10000, 10, 1),
(6, 'Very Awesome Product', 'Super duper awesome.', 10000, 12, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` enum('operator','admin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `level`, `provider`, `provider_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Vivi S Tuuk', 'vstuuk', 'vstuuk@gmail.com', '$2y$10$vt.IJ8AYJRwn7xgK07aebegI9OV5SkeNyRYxzKWQzqL8ABo2NsRWm', 'admin', 'local', '1', 'FVIVTrf818BnwD1WUCPbESstsVJhNV1cOrih2o7pS6L3fCOEcn7u9T1PdP8l', '2018-04-01 06:39:59', '2018-04-01 06:39:59'),
(4, 'Reymon R.', 'reyr19', NULL, NULL, 'operator', 'twitter', '47512353', 'XFQP5jMAXgDUZMNieMx1gh9iDIYO8BzBiZCgzx6I8r0OVwZYw2tRfI5Z7QvU', '2018-04-01 06:42:27', '2018-04-01 06:42:27'),
(5, 'Reymon Rotikan', 'reyr', 'rey.rotikan@gmail.com', '$2y$10$zGQH1k3dr5ZGwyeQirRII.zwkCdATNar0SB2OvdNPwHAP2Z5nOI0e', 'operator', 'local', '1', '2vQtxmDMNtsAJqXzlT2ORUQCmiD0Fmvj986jGZqPE4DGs6oywIBMGnq3PDND', '2018-04-02 02:10:07', '2018-04-02 02:10:07'),
(9, 'Mende Meisy', 'mmeisy', 'mmeisy@gmail.com', '$2y$10$jHOmblNRQGRI2p1CWym.ZOSFukvLu0dRJ2quJT2aMkuJSZ0UeMmKa', 'operator', 'local', '1', 'qwflouzxyXJHOR6wVlLBN5hUlABy6qjto0MuNYBKsvVWtANjG2SnIFX88Ccu', '2018-04-02 21:11:51', '2018-04-02 21:11:51'),
(11, 'Reymon Rotikan', 'reymonr', 'reymonr@unklab.ac.id', NULL, 'operator', 'google', '110027900536199698213', 'vVRsEPPGYyNBGp6X4DK1rEvAxSE67jazgDrbfgbgRHAbcfV2PUmpzuTFDffg', '2018-04-08 21:44:15', '2018-04-08 21:44:15'),
(12, 'Reymon Rotikan', 'reymonr', 'reymonr@gmail.com', '$2y$10$uxgKQlwLWiKAZL2ZgpI13e5hTeRkMi8zzPOq0N8sFizFjvdNkjrLq', 'admin', 'local', '1', 'F83f8DRUmME3Klzts8PCA1Awwi9Iu9kOiqahSUkzfuMd8LXwCEKc1YoaSdU3', '2018-05-15 19:51:42', '2018-05-15 19:51:42'),
(13, 'axel', 'axel', 'axel@mail.com', '$2y$10$oVz813c7OYMzGFE6x0gq3uCG7UayJ2iyimna2CPQQh.ibGF9EaOgS', 'operator', 'local', '1', 'El3zWQBSu7h3eosi0CxcnrjvSVUdretglLLtczeiXYew3ydOmPBSEwmSYodx', '2018-05-15 19:52:17', '2018-05-15 19:52:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`prod_id`),
  ADD KEY `products_prod_cat_id_foreign` (`prod_cat_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `prod_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_prod_cat_id_foreign` FOREIGN KEY (`prod_cat_id`) REFERENCES `categories` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
