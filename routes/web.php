<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PageController@index');
Route::get('/dashboard', 'DashboardController@index');
Route::get('/profile', 'ProfileController@index');


Route::resource('/events', 'EventController');
Route::resource('/categories', 'CategoryController');
Route::resource('/products', 'ProductController');


Auth::routes();
Route::get('/home', 'HomeController@index');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/social_login/{service}', 'SocialLogin@redirectToProvider');
Route::get('/callback/{service}', 'SocialLogin@handleProviderCallback');
