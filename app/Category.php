<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $primaryKey = 'cat_id';

	public $timestamps = false;

	protected $fillable = [
		'cat_name', 
		'cat_description',
	];	

	public function product()
	{
		return $this->hasMany('App\Product', 'prod_cat_id');
	}
}
