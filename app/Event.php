<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $primaryKey = 'event_id';

	public $timestamps = false;

	protected $fillable = [
		'event_title', 
		'event_speaker',
		'event_description',
		'event_fee',
		'event_category',
		'event_date'
	];	
}
