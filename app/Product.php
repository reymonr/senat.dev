<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $primaryKey = 'prod_id';

	public $timestamps = false;

	protected $fillable = [
		'prod_name',
		'prod_description',
		'prod_price',
		'prod_qty',
		'prod_cat_id',
	];	

	public function category()
	{
		return $this->belongsTo('App\Category', 'prod_cat_id');
	}
}
