<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Event;
use Socialite;

class DashboardController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth');
	}

    public function index()
    {
        $page = 'Dashboard';
        $events = Event::all();
        $tot = $events->count();
        $user = Auth::user();

    	return view('dashboard/index', compact('page', 'user', 'tot'));
    }
}
