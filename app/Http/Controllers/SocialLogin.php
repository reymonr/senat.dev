<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Event;
use App\User;
use Auth;

class SocialLogin extends Controller
{
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    public function handleProviderCallback($service)
    {
        $user = Socialite::driver($service)->user();
		$authUser = $this->findOrCreateUser($user, $service);
        Auth::login($authUser, true);

        $page = 'Dashboard';
        $events = Event::all();
        $tot = $events->count();

    	return view('dashboard/index', compact('page', 'user', 'tot'));
    }

     public function findOrCreateUser($user, $service)
     {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }

        return User::create([
            'name'     => $user->name,
            'username' => $user->getNickname(),
            'email'    => $user->getEmail(),
            'provider' => $service,
            'provider_id' => $user->id,
        ]);
     }
}
