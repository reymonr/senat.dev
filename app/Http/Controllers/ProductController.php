<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Event;
use App\Category;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$page = 'Products';
    	$prods = Product::all();
    	$events = Event::all();
        $tot = $events->count();
        $user = Auth::user();
    	return view('products/index', ['page' => $page, 'prods' => $prods, 'tot' => $tot, 'user' => $user]);
    }

    public function create()
    {
    	$page = 'Products';
    	$events = Event::all();
        $tot = $events->count();
        $category = Category::pluck('cat_name', 'cat_id');
        $user = Auth::user();
    	return view('products/create', ['page' => $page, 'tot' => $tot, 'cat' => $category, 'user' => $user]);
    }

    public function store(Request $req)
    {
        // validate data
        $validate_data = $req->validate([
            'prod_name' => 'required|max:255',
            'prod_description' => 'required',
            'prod_price' => 'required|integer',
            'prod_qty' => 'required|integer',
            'prod_cat_id' => 'required',
        ]);
        Product::create($req->all());
		return redirect('products');
    }

    public function edit($id)
    {
        $prod = Product::find($id);
        $page = 'Products';
        $tot = Event::all()->count();
        $cat = Category::pluck('cat_name', 'cat_id');
        $user = Auth::user();
        return view('products/edit', ['prod' => $prod, 'page' => $page, 'tot' => $tot, 'cat' => $cat, 'user' => $user]);
    }

    public function update($id, Request $req)
    {
        $prod = Product::find($id);

        $validateData = $req->validate([
            'prod_name' => 'required|max:255',
            'prod_description' => 'required',
            'prod_price' => 'required|integer',
            'prod_qty' => 'required|integer',
            'prod_cat_id' => 'required',
        ]);

        $prod->update($req->all());
        return redirect('products');
    }

    public function destroy($id)
    {
        $prod = Product::find($id);
        $prod->delete($prod);
        return redirect('products');
    }
}
