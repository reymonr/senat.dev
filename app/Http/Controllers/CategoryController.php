<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Event;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$page = 'Categories';
    	$cats = Category::all();
    	$events = Event::all();
        $tot = $events->count();
        $user = Auth::user();
    	return view('categories/index', ['page' => $page, 'cats' => $cats, 'tot' => $tot, 'user' => $user]);
    }

    public function create()
    {
    	$page = 'Categories';
    	$events = Event::all();
        $tot = $events->count();
        $user = Auth::user();
    	return view('categories/create', ['page' => $page, 'tot' => $tot, 'user' => $user]);
    }

    public function store(Request $req)
    {
        // validate data
        $validate_data = $req->validate([
            'cat_name' => 'required|max:255',
            'cat_description' => 'required',
        ]);
        Category::create($req->all());

		return redirect('categories');
    }

    public function edit($id)
    {
        $cat = Category::find($id);
        $page = 'Categories';
        $tot = Event::all()->count();
        $user = Auth::user();
        return view('categories/edit', ['cat' => $cat, 'page' => $page, 'tot' => $tot, 'user' => $user]);
    }

    public function update($id, Request $req)
    {
        $cat = Category::find($id);

        $validateData = $req->validate([
            'cat_name' => 'required|max:255',
            'cat_description' => 'required',
        ]);

        $cat->update($req->all());
        return redirect('categories');
    }

    public function destroy($id)
    {
        $cat = Category::find($id);
        $cat->delete($cat);
        return redirect('categories');
    }
}
