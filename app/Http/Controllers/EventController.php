<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	// step 1. get data from table
    	$events = Event::all();
        $page = 'Events';
        $tot = $events->count();
        $user = Auth::user();

    	// step 2. passing data into views
        return view('events/index', ['events' => $events, 'page' => $page, 'tot' => $tot, 'user' => $user]);
    }

    public function create() 
    {
        $page = 'Events';
        $tot = Event::all()->count();
        $user = Auth::user();
    	return view('events/create', ['page' => $page, 'tot' => $tot, 'user' => $user]);
    }

    public function store(Request $req)
    {
        // validate data
        $validate_data = $req->validate([
            'event_title' => 'required|max:255',
            'event_speaker' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'event_description' => 'required',
            'event_category' => 'required',
            'event_fee' => 'required|integer',
            'event_date' => 'required|after:today',
        ]);

        Event::create($req->all());

		return redirect('events');
    }

    public function edit($id)
    {
        $event = Event::find($id);
        $page = 'Events';
        $tot = Event::all()->count();
        $user = Auth::user();
        return view('events/edit', ['event' => $event, 'page' => $page, 'tot' => $tot, 'user' => $user]);
    }

    public function update($id, Request $req)
    {
        $event = Event::find($id);

        $validateData = $req->validate([
            'event_title' => 'required|max:255',
            'event_speaker' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'event_description' => 'required',
            'event_fee' => 'required|integer',
            'event_category' => 'required',
            'event_date' => 'required|date|after:today',
        ]);

        $event->update($req->all());
        return redirect('events');
    }

    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete($event);
        return redirect('events');
    }
}
