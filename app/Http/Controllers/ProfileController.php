<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Event;

class ProfileController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth');
	}

    public function index()
    {
        $page = 'Profile';
        $events = Event::all();
        $tot = $events->count();
        $user = Auth::user();

    	return view('profile/index', compact('page', 'user', 'tot'));
    }
}
