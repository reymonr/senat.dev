<?php

use Faker\Generator as Faker;

$factory->define(App\Event::class, function (Faker $faker) {
    return [
    	'event_title'=> $faker->sentence,
    	'event_speaker' => $faker->name,
    	'event_description' => $faker->realText(200),
		'event_fee' => $faker->numberBetween(100000, 250000),
		'event_category' => $faker->randomElement(['Chapel', 'Seminar', 'Workshop', 'Study Tour', 'Other']),
		'event_date' => $faker->date
    ];
});
