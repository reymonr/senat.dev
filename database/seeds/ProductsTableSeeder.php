<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
	    	[
	        	'prod_name'=> 'Samsung Galaxy S9',
	        	'prod_description' => 'New and modern smartphone.',
	        	'prod_price' => 11950000,
	        	'prod_qty' => 64,
	        	'prod_cat_id' => 1,
	        ],
	    	[
	        	'prod_name'=> 'Apple MacBook Air',
	        	'prod_description' => 'Your best friend for work.',
	        	'prod_price' => 14500000,
	        	'prod_qty' => 24,
	        	'prod_cat_id' => 2,
	        ],
	    	[
	        	'prod_name'=> 'Lenovo ThinkCenter Px1250',
	        	'prod_description' => 'Personal computer for office or server.',
	        	'prod_price' => 10500000,
	        	'prod_qty' => 128,
	        	'prod_cat_id' => 3,
	        ],
	    	[
	        	'prod_name'=> 'DELL Latitude ESX 420',
	        	'prod_description' => 'Personal computer for multimedia.',
	        	'prod_price' => 17500000,
	        	'prod_qty' => 12,
	        	'prod_cat_id' => 3,
	        ]
    	]);
    }
}
