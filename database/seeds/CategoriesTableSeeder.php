<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
	    	[
	        	'cat_name'=> 'Smartphone',
	        	'cat_description' => 'A celular phone for your work.',
	        ],
	    	[
	        	'cat_name'=> 'Laptop',
	        	'cat_description' => 'A portable computer for daily uses.',
	        ],
	    	[
	        	'cat_name'=> 'PC',
	        	'cat_description' => 'Personal computer for office or server.',
	        ]
    	]);
    }
}
