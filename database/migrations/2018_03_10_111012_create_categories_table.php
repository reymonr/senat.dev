<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('cat_id');
            $table->string('cat_name');
            $table->text('cat_description');
        });

        // set FK di kolom prod_cat_id di tabel products
        Schema::table('products', function(Blueprint $table) {
            $table->foreign('prod_cat_id')
                  ->references('cat_id')
                  ->on('categories')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop FK di kolom prod_cat_id di tabel products
        Schema::table('products', function(Blueprint $table) {
            $table->dropForeign('products_prod_cat_id_foreign');
        });

        Schema::dropIfExists('categories');
    }
}
