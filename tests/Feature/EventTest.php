<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testInsert()
    {
        // $this->assertTrue(true);
        factory(\App\Event::class, 1)->create([
        	'event_title' => 'Unit Test',
        ]);

        return $this->assertDatabaseHas('events', [
        	'event_title' => 'Unit Test',
        ]);
    }
}
