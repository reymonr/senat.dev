<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
            </a>
            <a class="brand" href="{{ url('/dashboard') }}">Senat.FIK</a>
            <div class="nav-collapse collapse">
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> 
                            <i class="icon-user"></i>{{ $user->name }}<i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="{{ url('/profile') }}">Profile</a></li>
                            <li><a tabindex="-1" href="{{ url('/profile') }}">Change Password</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="{{ url('/logout') }}">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
