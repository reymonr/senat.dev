<title>senat.dev | dashboard</title>

<link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
<link href="{{ asset('assets/bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" media="screen">
<link href="{{ asset('assets/vendors/easypiechart/jquery.easy-pie-chart.css') }}" rel="stylesheet" media="screen">
<link href="{{ asset('assets/assets/styles.css') }}" rel="stylesheet" media="screen">

<link rel="shortcut icon" href="{{ asset('favicon_fik.ico') }}" type="image/x-icon" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script src="{{ asset('assets/vendors/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
