<div class="span3" id="sidebar">
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
	        <li @if($page == 'Dashboard') ? class="active" : '' @endif><a href="{{ url('/dashboard') }}"><i class="icon-chevron-right"></i> Dashboard</a></li>
	        <li @if($page == 'Events') ? class="active" : '' @endif><a href="{{ url('/events') }}"><i class="icon-chevron-right"></i> <!-- <span class="badge badge-success pull-right"> {{ $tot }} </span> --> Events</a></li>
	        <li @if($page == 'Categories') ? class="active" : '' @endif><a href="{{ url('/categories') }}"> <i class="icon-chevron-right"></i> Categories</a></li>
	        <li @if($page == 'Products') ? class="active" : '' @endif><a href="{{ url('/products') }}"> <i class="icon-chevron-right"></i> Products</a></li>
    </ul>
</div>
