<script src="{{ asset('assets/vendors/jquery-1.9.1.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendors/easypiechart/jquery.easy-pie-chart.js') }}"></script>
<script src="{{ asset('assets/assets/scripts.js') }}"></script>

<link href="{{ asset('assets/vendors/wysiwyg/bootstrap-wysihtml5.css') }}" rel="stylesheet" media="screen">
<script src="{{ asset('assets/vendors/wysiwyg/wysihtml5-0.3.0.js') }}"></script>
<script src="{{ asset('assets/vendors/wysiwyg/bootstrap-wysihtml5.js') }}"></script>

<script>
  	function ConfirmDelete() {
  		var x = confirm("Are you sure want to delete this?");
  		if (x)
    		return true;
  		else
    		return false;
  	}
</script>
