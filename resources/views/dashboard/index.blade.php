@extends('master')

@section('content')
<div class="span9" id="content">
    <div class="row-fluid">
        <p>&nbsp;</p>
    	<div class="navbar">
        	<div class="navbar-inner">
                <ul class="breadcrumb">
                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <li><a href="#">Dashboard</a></li>
                </ul>
        	</div>
    	</div>
	</div>

    <div class="row-fluid">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">Welcome</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <h4>Dear {{ $user->name }}</h4>
                    <p>Welcome to your dashboard page.</p>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
@endsection
