@extends('master')

@section('content')
<div class="span9" id="content">
    <div class="row-fluid">
        <p>&nbsp;</p>
    	<div class="navbar">
        	<div class="navbar-inner">
                <ul class="breadcrumb">
                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <li><a href="{{ url('/dashboard') }}">Dashboard</a><span class="divider">/</span></li>
                    <li><a href="{{ url('/events') }}">Events</a><span class="divider">/</span></li>
                    <li>Edit Event</li>
                </ul>
        	</div>
    	</div>
	</div>

	<div class="row-fluid">
		<div class="block">
    		<div class="navbar navbar-inner block-header">
        		<div class="muted pull-left">Form Edit Event</div>
    		</div>
    		<div class="block-content collapse in">
        		<div class="span12">
					{!! Form::model($event, 
						[
							'method' => 'PATCH', 
							'action' => ['EventController@update', $event->event_id],
							'class' => 'form-horizontal'
						]) 
					!!}
                                <div class="control-group">
                				{{ Form::label('event_title', 'Event Title', ['class' => 'control-label']) }}
                  				<div class="controls">
                  					{{ Form::text('event_title', null, ['class' => 'span6', 'autofocus' => '']) }}
                  				</div>
                			</div>

                                <div class="control-group">
                  				{{ Form::label('event_speaker', 'Event Speaker', ['class' => 'control-label']) }}
                  				<div class="controls">
                    				{{ Form::text('event_speaker', null, ['class' => 'span6']) }}
                  				</div>
                			</div>

                                <div class="control-group">
                  				{{ Form::label('event_description', 'Event Description', ['class' => 'control-label']) }}
                  				<div class="controls">
                    				<!-- <textarea class="input-xlarge textarea" placeholder="Enter text ..." style="width: 810px; height: 200px"></textarea> -->
                    				{{ Form::textarea('event_description', null, ['class' => 'input-xlarge textarea', 'style' => 'width: 810px; height: 200px']) }}
                  				</div>
                			</div>

                                <div class="control-group">
                  				{{ Form::label('event_fee', 'Event Fee', ['class' => 'control-label']) }}
                  				<div class="controls">
                    				{{ Form::text('event_fee', null, ['class' => 'span6']) }}
                  				</div>
                			</div>

                                <div class="control-group">
                  				{{ Form::label('event_category', 'Event Category', ['class' => 'control-label']) }}
                  				<div class="controls">
                  					{{ Form::checkbox('event_category', 'Chapel') }}  Chapel
									{{ Form::checkbox('event_category', 'Seminar') }} Seminar
									{{ Form::checkbox('event_category', 'Workshop') }} Workshop
									{{ Form::checkbox('event_category', 'Study Tour') }} Study Tour
									{{ Form::checkbox('event_category', 'Other') }} Other
                  				</div>
                			</div>

                                <div class="control-group">
                  				{{ Form::label('event_date', 'Event Date', ['class' => 'control-label']) }}
                  				<div class="controls">
                    				{{ Form::date('event_date', null, ['class' => 'span6']) }}
                  				</div>
                			</div>

                			<div class="form-actions">
                  				<a href="{{ url('/events') }}" class="btn">Cancel</a>
                				{{ Form::submit('Update Event', ['class' => 'btn btn-primary']) }}
                			</div>
            		{!! Form::close() !!}
        		</div>
    		</div>
		</div>
	</div>
</div>
@endsection
