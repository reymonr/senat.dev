@extends('master')

@section('content')
<div class="span9" id="content">
    <div class="row-fluid">
        <p>&nbsp;</p>
    	<div class="navbar">
        	<div class="navbar-inner">
                <ul class="breadcrumb">
                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <li><a href="{{ url('/dashboard') }}">Dashboard</a><span class="divider">/</span></li>
                    <li><a href="{{ url('/categories') }}">Categories</a><span class="divider">/</span></li>
                    <li><a href="#">Add Category</a></li>
                </ul>
        	</div>
    	</div>
	</div>

	<div class="row-fluid">
		<div class="block">
    		<div class="navbar navbar-inner block-header">
        		<div class="muted pull-left">Form Add Category</div>
    		</div>
    		<div class="block-content collapse in">
        		<div class="span12">

              @if ($errors->any())
                <div class="alert alert-error alert-block">
                  <a class="close" data-dismiss="alert" href="#">&times;</a>
                  <h4 class="alert-heading">Something is wrong!</h4>
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li> {{ $error }} </li>
                      @endforeach
                    </ul>
                </div>
              @endif

        			{!! Form::open(['url' => 'categories', 'class' => 'form-horizontal']) !!}
                      <div class="control-group">
            				    {{ Form::label('cat_name', 'Category Name', ['class' => 'control-label']) }}
                                <div class="controls">
              					{{ Form::text('cat_name', '', ['class' => 'span6', 'autofocus' => '']) }}
                      </div>
                			</div>
                        <div class="control-group">
                  				{{ Form::label('cat_description', 'Description', ['class' => 'control-label']) }}
                  				<div class="controls">
                    				{{ Form::textarea('cat_description', '', ['class' => 'input-xlarge textarea', 'style' => 'width: 810px; height: 200px']) }}
                  				</div>
                			</div>

                			<div class="form-actions">
                  				<a href="{{ url('/categories') }}" class="btn">Cancel</a>
                				{{ Form::submit('Add Category', ['class' => 'btn btn-primary']) }}
                			</div>
            		{!! Form::close() !!}
        		</div>
    		</div>
		</div>
	</div>
</div>
@endsection
