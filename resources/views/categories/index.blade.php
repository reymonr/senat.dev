@extends('master')

@section('content')
<div class="span9" id="content">
    <div class="row-fluid">
        <p>&nbsp;</p>
    	<div class="navbar">
        	<div class="navbar-inner">
                <ul class="breadcrumb">
                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <li><a href="{{ url('/dashboard') }}">Dashboard</a><span class="divider">/</span></li>
                    <li><a href="{{ url('/categories') }}">Categories</a></li>
                </ul>
        	</div>
    	</div>
	</div>

    <div class="row-fluid">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">List of Category</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                	<div class="btn-group">
                		@if (Auth::user()->level == 'admin')
                        <a href="{{ url('/categories/create') }}"><button class="btn btn-success">Add Category <i class="icon-plus icon-white"></i></button></a>
                        @endif
                	</div>
					<table class="table">
		              	<thead>
		              		@if (Auth::user()->level == 'admin')
			                	<tr>
			                  		<th>No</th>
			                  		<th>Name</th>
			                  		<th>Description</th>
			                  		<th>Action</th>
			                	</tr>
		              		@else
		                	<tr>
		                  		<th>No</th>
		                  		<th>Name</th>
		                  		<th>Description</th>
		                	</tr>
		                	@endif
		              	</thead>
		              	<tbody>
							<?php $no=1; ?>
							@foreach($cats as $cat)
			                	<tr>
									@if (Auth::user()->level == 'admin')
										<td>{{ $no }}</td>
										<td>{{ $cat->cat_name }}</td>
										<td>{{ $cat->cat_description }}</td>
										<td>
											{{ link_to('categories/'.$cat->cat_id.'/edit', 'Edit', ['class' => 'btn btn-mini']) }} 
											{!! Form::open(['action' => ['CategoryController@destroy', $cat->cat_id], 'onsubmit' => 'return ConfirmDelete()']) !!}
												{{ Form::hidden('_method', 'DELETE') }}
												{{ Form::submit('Delete', ['class' => 'btn btn-danger btn-mini']) }}
											{!! Form::close() !!}
										</td>
									@else
									<td>{{ $no }}</td>
									<td>{{ $cat->cat_name }}</td>
									<td>{{ $cat->cat_description }}</td>
									@endif
			                	</tr>
			                	<?php $no++; ?>
		                	@endforeach
		              	</tbody>
		            </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
