<!DOCTYPE html>
<html class="no-js">    
    <head>
        @include('includes.header')
    </head>
    <body>
        @include('includes.topbar')

        <div class="container-fluid">
            <div class="row-fluid">
                @include('includes.sidebar')
                @yield('content')
            </div>
            @include('includes.footer')
        </div>

        @include('includes.scripts')
    </body>
</html>
