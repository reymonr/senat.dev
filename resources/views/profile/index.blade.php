@extends('master')

@section('content')
<div class="span9" id="content">
    <div class="row-fluid">
        <p>&nbsp;</p>
    	<div class="navbar">
        	<div class="navbar-inner">
                <ul class="breadcrumb">
                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
                    <li><a href="{{ url('/dashboard') }}">Dashboard</a><span class="divider">/</span></li>
                    <li><a href="#">Profile</a></li>
                </ul>
        	</div>
    	</div>
	</div>

	<div class="row-fluid">
		<div class="block">
    		<div class="navbar navbar-inner block-header">
        		<div class="muted pull-left">User Profile</div>
    		</div>
    		<div class="block-content collapse in">
        		<div class="span12">
              <form class="form-horizontal">
                <fieldset>
                  <legend>User Profile</legend>
                  <div class="control-group">
                    <label class="control-label" for="focusedInput">Full Name</label>
                    <div class="controls">
                      <input class="input-xlarge uneditable-input" type="text" value="{{ $user->name }}">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="focusedInput">Username</label>
                    <div class="controls">
                      <input class="input-xlarge uneditable-input" type="text" value="{{ $user->username }}">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="focusedInput">Email Address</label>
                    <div class="controls">
                      <input class="input-xlarge uneditable-input" type="text" value="{{ $user->email }}">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="focusedInput">Level</label>
                    <div class="controls">
                      <input class="input-xlarge uneditable-input" type="text" value="{{ $user->level }}">
                    </div>
                  </div>
                </fieldset>
              </form>
        		</div>
    		</div>
		</div>
	</div>
</div>
@endsection
